DIR=template01
CLEAN_NAME=AdamSalwowski_CV
FILE_PATH=$(DIR)/$(CLEAN_NAME)
CV_PL=$(FILE_PATH)-pl.pdf
CV_EN=$(FILE_PATH)-en.pdf
TMP=/tmp/cv_merged.pdf

PHONY: compress

all: compress

compress:
	gs -dNOPAUSE -sDEVICE=pdfwrite  -dBATCH -sOUTPUTFILE=$(TMP) $(CV_PL) $(CV_EN)
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$${HOME}/Desktop/AdamSalwowski_CV.pdf $(TMP)
